<?php

/*
 * (c) www.wunderbit.co
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace Wbc;

/**
 * Class WbcSDK
 * @package Wbc\WbcSDK
 */
class WbcSDK
{
    const CURRENCY_BTC = 'BTC';

    /**
     * @var integer
     */
    private $apiKey;

    /**
     * @var string
     */
    private $generateUrl = 'https://cabinet.commerce.wunderbit.co/api/v1/invoice/generate';

    /**
     * @var string
     */
    private $showInvoiceUrl = 'https://cabinet.commerce.wunderbit.co/api/v1/invoice/show/';

    function __construct($apiKey)
    {
        // Check cURL extension availability.
        $ch = curl_init();
        curl_close($ch);

        $this->apiKey = $apiKey;
    }

    /**
     * @param string $customerEmail max 255 characters
     * @param float $totalPrice e.g. 3422.43
     * @param string $customId max 255 characters
     * @param array $items
     *      - array of items, e.g. item is ['item_id' => 3, 'item_name' => 'milk', 'item_price' => 200, 'item_amount' => 5]
     *      - 'Max size of "items" array is 999'
     *      - Item properties 'item_id', 'item_name', 'item_price', 'item_amount' are mandatory.
     * @param array $extra - extra properties of invoice, e.g. ['customer_phone' => '+372 2343345', 'delivery_method' => 'postal']
     * @param string $comebackUrl - url for button 'come back to merchant' on invoice page.
     * @param string $paidCallback - url callback, called when invoice is paid.
     * @param string $currency - with which currency buyer will pay - BTC (default).
     * @return mixed
     */
    public function generateInvoice(
        $customerEmail,
        $totalPrice,
        $customId = null,
        $items = [],
        $extra = [],
        $comebackUrl = null,
        $paidCallback = null,
        $currency = self::CURRENCY_BTC
    )
    {
        $params = [
            'api_key' => $this->apiKey,
            'customer_email' => $customerEmail,
            'total_price' => $totalPrice,
            'custom_id' => $customId,
            'items' => $items,
            'extra' => $extra,
            'comeback_url' => urlencode($comebackUrl),
            'paid_callback' => urlencode($paidCallback),
            'currency' => $currency
        ];

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $this->generateUrl);

        curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-type: application/json']); // Requesting JSON.
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); // Return into variable without sending to client.

        // Send POST request.
        curl_setopt($ch, CURLOPT_POST, 1);
        // Send as a form.
        curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/x-www-form-urlencoded']);

        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($params));

        $response = curl_exec($ch);
        curl_close($ch);

        // If using JSON...
        $data = json_decode($response);

        return $data;
    }

    /**
     * @param string $invoiceCode
     * @return string
     */
    public function getInvoiceUrl($invoiceCode)
    {
        return $this->showInvoiceUrl . $invoiceCode;
    }

    private function validate()
    {

    }

    /**
     * @return int
     */
    public function getApiKey()
    {
        return $this->apiKey;
    }

    /**
     * @param string $generateUrl
     */
    public function setGenerateUrl($generateUrl)
    {
        $this->generateUrl = $generateUrl;
    }

    /**
     * @param string $showInvoiceUrl
     */
    public function setShowInvoiceUrl($showInvoiceUrl)
    {
        $this->showInvoiceUrl = $showInvoiceUrl;
    }
}
